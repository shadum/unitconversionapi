import jwt
from flask import jsonify, request
from routes.common import init_logger
from constant import *

def validate_token():
    try:
        logger = init_logger()
        req_secret = request.headers['SECRET']

        decode = jwt.decode(req_secret, ENCRYPT_KEY, algorithm=ALGORITHM)

        if ENCRYPT_DETAIL != decode['encrypt_detail']:
            logger.error('Validate key false')
            return jsonify('UNAUTHORIZED'), UNAUTHORIZED
    except:
        return jsonify('UNAUTHORIZED'), UNAUTHORIZED
