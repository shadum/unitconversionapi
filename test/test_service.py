import unittest
from routes.service import *


class TestService(unittest.TestCase):
    def test_unit_convert(self):
        formula = "y = x + 2"
        self.assertEqual(unit_convert(2, formula), 4)

        formula = "y = 2*x + 2"
        self.assertEqual(unit_convert(2, formula), 6)

        formula = "y = 2x + 2"
        self.assertFalse(unit_convert(2, formula))

        formula = "x+2"
        self.assertFalse(unit_convert(2, formula))

        formula = "y = abc+ x+ 2"
        self.assertFalse(unit_convert(2, formula))

    def test_sanitize(self):
        self.assertEqual(sanitize_input({'data': ' I have a pen. '}), {'data': 'I have a pen.'})
        self.assertEqual(sanitize_input({'data': 5}), {'data': 5})
        self.assertEqual(sanitize_input({'data1': 5, 'data2': ' I have a pen. '}),
                         {'data1': 5, 'data2': 'I have a pen.'})
        self.assertEqual(sanitize_input({'data': '<div>'}), {'data': '&lt;div&gt;'})

    def test_check_has_required_key(self):
        required_key = ['a', 'b']
        self.assertFalse(check_has_required_key({}, required_key))
        self.assertFalse(check_has_required_key({'a': 'apple'}, required_key))
        self.assertFalse(check_has_required_key({'b': 'banana'}, required_key))
        self.assertTrue(check_has_required_key({'a': 'apple', 'b': 'banana'}, required_key))
        self.assertTrue(check_has_required_key({'a': 'apple', 'b': 'banana', 'c': 'coconut'}, required_key))
        self.assertFalse(check_has_required_key({'a': 'apple', 'c': 'coconut'}, required_key))

    def test_check_has_over_available_key(self):
        available_key = ['a', 'b']
        # self.assertFalse(check_has_over_available_key({}, available_key))
        self.assertFalse(check_has_over_available_key({'a': 'apple'}, available_key))
        self.assertFalse(check_has_over_available_key({'a': 'apple', 'b': 'banana'}, available_key))
        self.assertTrue(check_has_over_available_key({'a': 'apple', 'b': 'banana', 'c': 'coconut'}, available_key))


if __name__ == '__main__':
    unittest.main()
