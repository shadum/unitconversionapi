import mysql.connector as db_connector
from constant import *

if __name__ == '__main__':

    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if conn.is_connected():
        cursor = conn.cursor()
        f = open("schema/sample_data.sql", "r")
        statement = f.read()
        print(statement)
        cursor.execute(statement, multi=True)
        conn.commit()
        print('finish')
