# Unit Conversion API
This project is created for HG Robotics as a part of interview process.

This README describes about how to set up and use this api on local environment.

## Prerequisite

The computer (or server) running this project has to install these components before starting and make sure you have an internet connection
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [make](https://stackoverflow.com/questions/10265742/how-to-install-make-and-gcc-on-a-mac)

And make sure that there is no any service running on port: 3350 and 3351

## About the project

This project uses Flask as a main Framework, MariaDB as a database and builds everything on Docker. 
You don't need to install Flask and any required library in your local machine except the things mentioned on prerequisite part.

## Set up the project

clone the project to any directory you plan to build e.g. /opt/ and then change your working directory into the project
```
cd /opt/

git clone https://<your_bitbucket_username>@bitbucket.org/shadum/unitconversionapi.git

cd /unitconversionapi
```

build docker image from Dockerfile using make to help you abbreviate command
```
make build
```
or if you have no Make software, you can see what 'make build' do by reading in Makefile

after building docker image, there will be an image name unit-conversion-api:1.0 on your local machine. You can inspect by command
```
docker image ls
```

To run this docker image, using make file to run it up
```
make up
```
This command will call docker-compose to run unit-conversion-api image and mariadb image as a database

After running up the command please wait for a few moments before running the next command. (Database need time in first running)

By run make up command, your project is ready for getting request. But the database is not ready because there is no table in database.

There is sql file to create table and sample data located in unitconversionapi/schema

There are several ways to run this sql file, to create table and insert sample data, 
such as using database client software, exec into docker and using mysql command or using Makefile to help everything easier
```
make add-schema
make add-sample-data
```
If script is not working, you can try using other method to dump data into DB.

Setting up project is completed right now and ready to receive request.
By default, Flask service is running on port 3350 and database is running on port 3351
Database default config; username: root, password: password, database_name: mainDb

## Using api

You can use many ways to call api in this project such as using software like postman, insomnia or using curl command in terminal

below shows example of using curl command to call GET api
```
curl -X GET localhost:3350/unit
-H "Content-Type:application/json" 
-H "SECRET:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbmNyeXB0X2RldGFpbCI6Imlsb3ZlYWkifQ.Y0TZw4KYwr0btEFxYPovN8XcUYylddPpwXGK4aN8ezA" 
```
This command is calling localhost:3350/unit by setting header to "Content-Type:application/json" and "SECRET:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbmNyeXB0X2RldGFpbCI6Imlsb3ZlYWkifQ.Y0TZw4KYwr0btEFxYPovN8XcUYylddPpwXGK4aN8ezA" as an authentication.

below shows example of using curl command to call POST api
```
curl -X POST localhost:3350/unit 
-H "Content-Type:application/json" 
-H "SECRET:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbmNyeXB0X2RldGFpbCI6Imlsb3ZlYWkifQ.Y0TZw4KYwr0btEFxYPovN8XcUYylddPpwXGK4aN8ezA" 
-d '{"unit": "farenheight","type": "temperature"}'
```
This command is calling localhost:3350/unit by setting header to "Content-Type:application/json" and "SECRET:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbmNyeXB0X2RldGFpbCI6Imlsb3ZlYWkifQ.Y0TZw4KYwr0btEFxYPovN8XcUYylddPpwXGK4aN8ezA" as an authentication
and sending data to api to add new row in Unit database.

## API reference

### Header
every route of this api need authentication header (except only route "/")
```
SECRET:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbmNyeXB0X2RldGFpbCI6Imlsb3ZlYWkifQ.Y0TZw4KYwr0btEFxYPovN8XcUYylddPpwXGK4aN8ezA
```
The body of the secret is created by JWT authentication by encoding data "iloveai" with a encrypt key "secret"
read more about [JWT](https://jwt.io/)

### Get all unit in database
```
GET localhost:3350/unit
```

### Get a unit in database by id
```
GET localhost:3350/unit/{id}
```

### Get all formula in database
```
GET localhost:3350/formula
```
example of return response
```
{
	"data": [
		[
			"{\"id\": 1, \"unit\": \"meter\", \"type\": \"length\"}"
		],
		[
			"{\"id\": 3, \"unit\": \"square metre\", \"type\": \"area\"}"
		],
		[
			"{\"id\": 4, \"unit\": \"square inch\", \"type\": \"area\"}"
		]
	]
}
```

### Get a formula in database
```
GET localhost:3350/formula/{id}
```

### Create a new row of unit in database
```
POST localhost:3350/unit
```
with body
```
{
	"unit": "farenheight",
	"type": "temperature"
}
```
"unit" and "type" are required fields. unit is the name of the unit you want and type is the type of that unit e.g. length, mass, temperature, etc.

example of return response
```
{
	"message": "create new data on table unit finish."
}
```

### Create a new row of unit in database with the specified id
```
POST localhost:3350/unit
```
with body
```
{
    "id": 150,
	"unit": "farenheight",
	"type": "temperature"
}
```
By default, "id" field is auto increment field, you don't need to tell db.

### Update unit data of existing row in database with the specified id
```
POST localhost:3350/unit
```
with body
```
{
    "id": 150,
	"unit": "kelvin",
	"type": "temperature"
}
```
This will update data to row id=150. If there is no data on that id, it will create one instead.

### Delete unit data of existing row in database with the specified id
```
DELETE localhost:3350/unit
```
with body
```
{
    "id": 150
}
```

### Create a new row of formula in database
```
POST localhost:3350/formula
```
with body
```
{
	"unitFromId": "1",
	"unitToId": "2",
	"formula": "y=x+273"
}
```
"unitFromId", "unitToId" and "formula" are required fields. 
unitFromId is the id of the unit (from unit table) you want to convert from (variable "x" in formula)
unitToId is the id of the unit (from unit table) you want to convert to (variable "y" in formula)
formula is the convert formula ("y", "x" and "=" are required)


### Create a new row of formula in database with the specified id
```
POST localhost:3350/formula
```
with body
```
{
    "id": 150
	"unitFromId": "1",
	"unitToId": "2",
	"formula": "y=x+273"
}
```
By default, "id" field is auto increment field, you don't need to tell db.

### Update formula data of existing row in database with the specified id
```
POST localhost:3350/formula
```
with body
```
{
    "id": 150
	"unitFromId": "1",
	"unitToId": "2",
	"formula": "y=x+273"
}
```
This will update data to row id=150. If there is no data on that id, it will create one instead.

### Delete unit data of existing row in database with the specified id
```
DELETE localhost:3350/formula
```
with body
```
{
    "id": 150
}
```

### Convert unit
```
POST localhost:3350/calculate
```
with body
```
{
	"value": "37"
	"unitFromId": "1",
	"unitToId": "2",
}
```
For example if id 1 from unit table means celsius in temperature, id 2 means kelvin
this request will find formula to convert 37 degree celsius to kelvin unit in formula table
and will return 37 + 273 = "310" Kelvin as a response message.
example of return response
```
{
	"data": "310"
}
```

*** Caution ***
This version of api, has no process to prevent adding more than a formula in the same id.
For example in formula table, there is existed formula to convert celsius to kelvin and then user add more row formula by the same unitFromId and unitToId.
when sending calculate request, response will show below message and calculate nothing.
```
{
	"message": "not found or there are more than one formula matched"
}
```

## Run unit test
To run unit test, you can use make command
```
make unit
```

## Note
This project is not ready-to-use project in production. 
There are a lot of things to improve such as changing the library running raw sql command to library that has ORM concept to prevent SQL injection,
more detail for security, cast type of input before processing etc.
It just an OK project for interview assignment. 

All code is tested on Mac (macBookAir M1 with macOS Big Sur:11.2.3). 
if anything not work in your local machine, please tell me, I will help you find out.  