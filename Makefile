docker_name := unit-conversion-api
tag := 1.0

build:
	docker build -t ${docker_name}:${tag} .
up:
	docker-compose -f ./local-docker-compose.yml up -d
down:
	docker-compose -f ./local-docker-compose.yml down
add-schema:
	docker exec -it unit-conversion-api python add_schema.py
add-sample-data:
	docker exec -it unit-conversion-api python add_sample_data.py
unit:
	docker exec -it unit-conversion-api python -m unittest test/test_service.py