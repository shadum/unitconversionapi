PATH_PREFIX = '/opt/unitconversionapi'
LOG_PREFIX = '/opt/unitconversionapi/logs'
DB_HOSTNAME = 'unit-conversion-api_db'
DB_USER = 'root'
DB_PASSWORD = 'password'
DB_NAME = 'mainDb'
DB_PORT = 3306

# Status code
OK = 200
BAD_REQUEST = 400
UNAUTHORIZED = 401
NOTFOUND = 404
INTERNAL_ERROR = 500

# Secret
ENCRYPT_KEY = 'secret'
ALGORITHM = 'HS256'
ENCRYPT_DETAIL = 'iloveai'
