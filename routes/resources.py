from routes.common import init_logger
from routes.service import *
from constant import *
from flask import jsonify, request
import json
import mysql.connector as db_connector


def unit_action():
    logger = init_logger()
    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if not conn.is_connected():
        return {'message': 'internal server error.'}, INTERNAL_ERROR
    cursor = conn.cursor()

    if request.method == 'GET':
        statement = f'SELECT JSON_OBJECT("id", id,"unit", unit,"type", type) FROM unit'
        cursor.execute(statement)
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return {'data': result}, OK
    if request.method == 'POST':
        data = sanitize_input(json.loads(request.data))
        required_key = ['unit', 'type']
        available_key = ['id', 'unit', 'type', 'metaDataJson', 'createdAt', 'updatedAt']
        if not check_has_required_key(data, required_key):
            message = ','.join(required_key)
            return {'message': f'Input required key: {message}.'}, BAD_REQUEST
        if check_has_over_available_key(data, available_key):
            message = ','.join(available_key)
            return {'message': f'Input available only these key: {message}.'}, BAD_REQUEST

        if 'id' not in data:
            statement, val = create_insert_statement(data, 'unit')
            logger.debug(statement, val)
            print(statement, val)
            cursor.execute(statement, val)
            conn.commit()
            return {'message': f'create new data on table unit finish.'}, OK

        cursor.execute(f'SELECT COUNT(*) as count_check FROM unit WHERE id={data["id"]} LIMIT 1')
        for count_check in cursor:
            # update statement
            if count_check[0] == 1:
                statement = create_update_statement(data, 'unit')
                logger.debug(statement)
                print(statement)
                cursor.execute(statement)
                conn.commit()
                return {'message': f'update data at id={data["id"]} on table unit finish.'}, OK
            # insert statement
            else:
                statement, val = create_insert_statement(data, 'unit')
                logger.debug(statement, val)
                print(statement, val)
                cursor.execute(statement, val)
                conn.commit()
                return {'message': f'create data at id={data["id"]} on table unit finish.'}, OK
    if request.method == 'DELETE':
        data = json.loads(request.data)
        if 'id' not in data:
            return {'message': 'bad request.'}, BAD_REQUEST
        statement = f'DELETE FROM unit WHERE id={data["id"]};'
        logger.debug(statement)
        cursor.execute(statement)
        conn.commit()
        return {'message': f'delete data at id={data["id"]} on table unit finish.'}, OK


def formula_action():
    logger = init_logger()
    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if not conn.is_connected():
        return {'message': 'internal server error.'}, INTERNAL_ERROR
    cursor = conn.cursor()

    if request.method == 'GET':
        statement = f'SELECT JSON_OBJECT("id",id,"unitFromId", unitFromId, "unitToId", unitToId, "formula", formula) ' \
                    f'FROM formula'
        cursor.execute(statement)
        result = cursor.fetchall()
        cursor.close()
        conn.close()
        return {'data': result}, OK
    if request.method == 'POST':
        data = sanitize_input(json.loads(request.data))
        required_key = ['unitFromId', 'unitToId', 'formula']
        available_key = ['id', 'unitFromId', 'unitToId', 'formula', 'metaDataJson', 'createdAt', 'updatedAt']
        if not check_has_required_key(data, required_key):
            message = ','.join(required_key)
            return {'message': f'Input required key: {message}.'}, BAD_REQUEST
        if check_has_over_available_key(data, available_key):
            message = ','.join(available_key)
            return {'message': f'Input available only these key: {message}.'}, BAD_REQUEST

        if 'id' not in data:
            statement, val = create_insert_statement(data, 'formula')
            logger.debug(statement, val)
            print(statement, val)
            cursor.execute(statement, val)
            conn.commit()
            return {'message': f'create new data on table formula finish.'}, OK

        cursor.execute(f'SELECT COUNT(*) as count_check FROM formula WHERE id={data["id"]} LIMIT 1')
        for count_check in cursor:
            # update statement
            if count_check[0] == 1:
                statement = create_update_statement(data, 'formula')
                logger.debug(statement)
                print(statement)
                cursor.execute(statement)
                conn.commit()
                return {'message': f'update data at id={data["id"]} on table formula finish.'}, OK
            # insert statement
            else:
                statement, val = create_insert_statement(data, 'formula')
                logger.debug(statement, val)
                print(statement, val)
                cursor.execute(statement, val)
                conn.commit()
                return {'message': f'create data at id={data["id"]} on table unit finish.'}, OK
    if request.method == 'DELETE':
        data = json.loads(request.data)
        if 'id' not in data:
            return {'message': 'bad request.'}, BAD_REQUEST
        statement = f'DELETE FROM unit WHERE id={data["id"]};'
        logger.debug(statement)
        cursor.execute(statement)
        conn.commit()
        return {'message': f'delete data at id={data["id"]} on table formula finish.'}, OK


def get_unit_by_id(id):
    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if not conn.is_connected():
        return {'message': 'internal server error.'}, INTERNAL_ERROR
    cursor = conn.cursor()
    statement = f'SELECT JSON_OBJECT("id", id,"unit", unit,"type", type) FROM unit WHERE id={id}'
    cursor.execute(statement)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return {'data': result}, OK


def get_formula_by_id(id):
    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if not conn.is_connected():
        return {'message': 'internal server error.'}, INTERNAL_ERROR
    cursor = conn.cursor()
    statement = f'SELECT JSON_OBJECT("id", id, "unitFromId", unitFromId, "unitToId", unitToId, "formula", formula) ' \
                f'FROM formula WHERE id={id}'
    cursor.execute(statement)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return {'data': result}, OK


def calculate():
    conn = db_connector.connect(user=DB_USER, password=DB_PASSWORD, database=DB_NAME, host=DB_HOSTNAME,
                                port=DB_PORT)
    if not conn.is_connected():
        return {'message': 'internal server error.'}, INTERNAL_ERROR
    cursor = conn.cursor()
    data = sanitize_input(json.loads(request.data))
    required_key = ['value', 'unitFromId', 'unitToId']
    if not check_has_required_key(data, required_key):
        message = ','.join(required_key)
        return {'message': f'Input required key: {message}.'}, BAD_REQUEST
    statement = f'SELECT formula, COUNT(*) as count_check ' \
                f'FROM formula WHERE unitFromId={data["unitFromId"]} AND unitToId={data["unitToId"]} LIMIT 1'
    cursor.execute(statement)
    for formula, count_check in cursor:
        print(formula, count_check)
        if count_check != 1:
            return {'message': 'not found or there are more than one formula matched'}, INTERNAL_ERROR
        value = unit_convert(data['value'], formula)
        if not value:
            return {'message': 'Problem while converting. Please check your input again.'}, BAD_REQUEST
        return {'data': value}, OK


if __name__ == '__main__':
    pass
