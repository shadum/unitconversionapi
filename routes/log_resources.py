from flask_restful import reqparse, Resource
from routes.common import init_logger
from os import listdir
from os.path import isfile, join
from constant import *

parser = reqparse.RequestParser()
parser.add_argument(
    'date', help='This field cannot be blank', required=True)


def get_list():
    logger = init_logger()
    log_path = LOG_PREFIX
    list_files = [f for f in listdir(log_path) if isfile(join(log_path, f))]
    logger.info(f'Received a getting log list request')
    return {'data': list_files}, 200


def get_log():
    logger = init_logger()
    try:
        data = parser.parse_args()
        date_read = data['date']

        filename = f'{LOG_PREFIX}/{date_read}'
        logger.info(f'Received a log POST request')

        if not check_file_exist(filename):
            logger.error(f'File not exist')
            return {'message': 'File not exist'}, 500

        with open(filename) as f:
            lines = f.read().splitlines()

        return {'data': lines}, 200

    except Exception as e:
        return {'message': f'Something wrong: {e}'}, 500


def check_file_exist(path):
    try:
        f = open(path)
        f.close()
        return True
    except FileNotFoundError:
        return False


if __name__ == '__main__':
    pass
