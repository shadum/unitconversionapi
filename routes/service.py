import html
import datetime


def sanitize_input(safe_data):
    for k, v in safe_data.items():
        if type(v) == str:
            safe_data[k] = html.escape(v).strip()
    return safe_data


def check_has_required_key(data, required_key_list):
    # check: is subset
    if set(required_key_list) <= set(data.keys()):
        return True
    return False


def check_has_over_available_key(data, available_key_list):
    # check: is subset
    if set(data.keys()) <= set(available_key_list):
        return False
    return True


def create_insert_statement(data, table_name):
    statement = f'INSERT INTO {table_name} ('
    val = []
    for key, value in data.items():
        statement = f'{statement} {key},'
        val.append(value)
    statement = f'{statement[:-1]}) VALUES ('
    for i in range(0, len(data)):
        statement = f'{statement}%s,'
    statement = f'{statement[:-1]})'
    return statement, val


def create_update_statement(data, table_name):
    statement = f'UPDATE {table_name} SET'
    for key, value in data.items():
        statement = f'{statement} {key} = "{value}",'
    update_timestamp = datetime.datetime.now().strftime("%Y-%m-%d %X")
    statement = f'{statement} updatedAt = "{update_timestamp}" WHERE id = {data["id"]};'
    return statement


def unit_convert(input_val, formula):
    try:
        formula = formula.split("=")[1].strip()
        input_val = float(input_val)
        output = eval(formula.replace("x", "input_val"))
        return output
    except:
        return False


if __name__ == '__main__':
    pass