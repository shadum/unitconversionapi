INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (1, 'meter', 'length', CURRENT_TIMESTAMP);
INSERT IGNORE
INTO unit (id, unit, type, createdAt)
VALUES (2, 'inch', 'length', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (3, 'square metre', 'area', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (4, 'square inch', 'area', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (5, 'cubic meter', 'volume', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (6, 'liter', 'volume', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (7, 'kilocalorie', 'energy', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (8, 'joule', 'energy', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (9, 'kilogram', 'mass', CURRENT_TIMESTAMP);
INSERT IGNORE INTO unit (id, unit, type, createdAt)
VALUES (10, 'pound', 'mass', CURRENT_TIMESTAMP);

INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (1, 1, 2,'y=x*39.37', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (2, 2, 1,'y=x/39.37', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (3, 3, 4,'y=x*1550', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (4, 4, 3,'y=x/1550', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (5, 5, 6,'y=x*1000', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (6, 6, 5,'y=x/1000', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (7, 7, 8,'y=x*4184', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (8, 7, 8,'y=x/4184', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (9, 9, 10,'y=x*2.205', CURRENT_TIMESTAMP);
INSERT IGNORE INTO formula (id, unitFromId, unitToId, formula, createdAt)
VALUES (10, 9, 10,'y=x/2.205', CURRENT_TIMESTAMP);