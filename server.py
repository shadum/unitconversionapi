from flask import Flask, Blueprint
from flask_restful import Api
from routes.resources import *
from routes.service import *
from routes.log_resources import *
from waitress import serve
from middleware.api_auth import validate_token

app = Flask(__name__)
api_bp = Blueprint('api_auth', __name__)
api = Api(api_bp)

# the only path with no need authentication header
@app.route('/')
def hello():
    return "Healthy."


api_bp.add_url_rule('/unit', methods=['GET', 'POST', 'DELETE'], view_func=unit_action)
api_bp.add_url_rule('/unit/<id>', methods=['GET'], view_func=get_unit_by_id)
api_bp.add_url_rule('/formula', methods=['GET', 'POST', 'DELETE'], view_func=formula_action)
api_bp.add_url_rule('/formula/<id>', methods=['GET', 'POST', 'DELETE'], view_func=get_formula_by_id)
api_bp.add_url_rule('/calculate', methods=['POST'], view_func=calculate)

app.before_request_funcs = {
    'api_auth': [validate_token]
}

app.register_blueprint(api_bp)

# Start the server with the 'run()' method
if __name__ == '__main__':
    # Development Environment
    # Set debug=True for development
    # app.run(debug=False, host='0.0.0.0', port='80')

    # Production
    serve(app, host='0.0.0.0', port='80')
