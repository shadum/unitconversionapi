FROM python:3.6.10-alpine

COPY requirements .
RUN pip install -r requirements
RUN rm requirements

EXPOSE 80